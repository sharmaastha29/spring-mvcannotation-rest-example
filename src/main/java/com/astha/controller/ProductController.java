package com.astha.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.astha.dao.ProductDao;
import com.astha.model.Product;

@RestController
public class ProductController {
	@Autowired
	private ProductDao pd;
	
	@GetMapping("/product")
	public List getAllProducts() {
		return pd.list();
	}
	
	@GetMapping("/product/{id}")
	public ResponseEntity getProduct(@PathVariable("id") int id){
		Product p=pd.get(id);
		if(p==null){
			return new ResponseEntity("No product found for id: "+ id, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(p, HttpStatus.OK);
	}
	
	@PostMapping("/product")
	public ResponseEntity createProduct(@RequestBody Product prd){
		pd.create(prd);

		System.out.println(prd);
		return new ResponseEntity(prd,HttpStatus.OK);
	}
	
	@DeleteMapping("/product/{id}")
	public ResponseEntity deleteProduct(@PathVariable("id") int id){
		int p=pd.delete(id);
		if(p==0){
			return new ResponseEntity("There is no product for id: " +id,HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(id+ " id deleted ", HttpStatus.OK);
	}
	
	
	@PutMapping("/product/{id}")
		public ResponseEntity updateProduct(@PathVariable("id") int id, @RequestBody Product p) {
		Product pr=pd.update(id, p);
		if(pr==null){
			return new ResponseEntity("can not update id :"+id,HttpStatus.NOT_FOUND);
		} 
		return new ResponseEntity(p, HttpStatus.OK);
	}

}
